using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManagerJ5 : MonoBehaviour
{
    public static bool gameOver = false;
    public static bool gameWin = false;
    // Start is called before the first frame update
    void Start()
    {
        gameOver = false;
        gameWin = false;

    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.R))
        {
            Time.timeScale = 1;
            SceneManager.LoadScene("Juego5");
        }
    }
}
