using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Controller_Player2 : MonoBehaviour
{
    public float fuerza;
    public float escalaGravedad;
    private Rigidbody rb;
    private bool isGravityDown = false;

   

    public bool enPiso = false;
    public bool gravedadInvertida = false;

    public bool activarGravedad = false;

   

    public int contSaltos = 0;

    public float velX = 3;
    public AudioClip audioSalto;
    private AudioSource audioSource;
    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody>();

        audioSource = GetComponent<AudioSource>();
      

       rb.useGravity = true;

      
    }

    // Update is called once per frame
    void Update()
    {
    

        if (enPiso)
        {
            rb.useGravity = false;

            activarGravedad = false;
            contSaltos = 0;
        }
        else
        {
            activarGravedad = true;
        }

        if (activarGravedad && gravedadInvertida)
        {
            GravedadInvertida();
        }
        else if (activarGravedad && !gravedadInvertida)
        {
            GravedadNormal();
        }
       
        


        if (Input.GetKeyDown(KeyCode.UpArrow)&&contSaltos<1)
        {
            

            gravedadInvertida = !gravedadInvertida;
            isGravityDown = !isGravityDown;
            Saltar();
         contSaltos++;
            
        }
        Vector3 velConstanteX = new Vector3(velX, rb.velocity.y, rb.velocity.z);


        rb.velocity = velConstanteX;
    }

    
   
    private void GravedadNormal()
    {
       
      
        rb.AddForce(Vector2.down * 9.8f, ForceMode.Acceleration);
    }

    private void GravedadInvertida()
    {
        
        rb.AddForce(Vector2.up * 9.8f, ForceMode.Acceleration);
       
    }

    private void Saltar()
    {
        Vector3 jumpDirection = isGravityDown ? Vector3.up : Vector3.down;
        rb.AddForce(jumpDirection * fuerza, ForceMode.VelocityChange);
        audioSource.PlayOneShot(audioSalto);
    }


    private void OnCollisionEnter(Collision collision)
    {

        if (collision.gameObject.tag == "Piso")
        {
            enPiso = true;
        }

        if (collision.gameObject.tag == "Barrera")
        {
            GameManagerJ5.gameOver = true;
        }

        if (collision.gameObject.tag == "BarreraWin")
        {
            GameManagerJ5.gameWin = true;
        }
    }


    private void OnCollisionExit(Collision collision)
    {
        if (collision.gameObject.tag == "Piso")
        {
            enPiso = false;
        }
    }

    private void FixedUpdate()
    {
        RaycastHit hitRightUp;



        if (Physics.Raycast(transform.position + new Vector3(0f, 0.45f, 0f), Vector3.right, out hitRightUp, 0.6f))
        {
            if (hitRightUp.collider.gameObject != null)
            {
                rb.velocity = new Vector3(0f, rb.velocity.y, rb.velocity.z);

            }

        }

        RaycastHit hitRightDown;
        if (Physics.Raycast(transform.position - new Vector3(0f, 0.45f, 0f), Vector3.right, out hitRightDown, 0.6f))
        {
            if (hitRightDown.collider.gameObject != null)
            {
                rb.velocity = new Vector3(0f, rb.velocity.y, rb.velocity.z);

            }

        }

        RaycastHit hitLeftUp;

        if (Physics.Raycast(transform.position + new Vector3(0f, 0.45f, 0f), Vector3.left, out hitLeftUp, 0.6f))
        {
            if (hitLeftUp.collider.gameObject != null)
            {

                rb.velocity = new Vector3(0f, rb.velocity.y, rb.velocity.z);
            }

        }
        RaycastHit hitLeftDown;

        if (Physics.Raycast(transform.position - new Vector3(0f, 0.45f, 0f), Vector3.left, out hitLeftDown, 0.6f))
        {
            if (hitLeftDown.collider.gameObject != null)
            {

                rb.velocity = new Vector3(0f, rb.velocity.y, rb.velocity.z);
            }

        }
       
        
        if (hitRightUp.collider.gameObject.tag == "BarreraWin" ||
            hitRightDown.collider.gameObject.tag == "BarreraWin" ||
            hitLeftUp.collider.gameObject.tag == "BarreraWin" ||
            hitLeftDown.collider.gameObject.tag == "BarreraWin")
        {
            GameManagerJ5.gameWin = true;
        }

    }
}
