using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class ControlUI : MonoBehaviour
{
    public TMPro.TMP_Text txtGameOver;
    public TMPro.TMP_Text txtGameWin;

    public AudioClip audioGameOver;
    private AudioSource audioS;

    private int timer = 0;
    private int timer2 = 0;
    void Start()
    {
        txtGameOver.enabled = false;
        txtGameWin.enabled = false;
        audioS = GetComponent<AudioSource>();
    }

    // Update is called once per frame
    void Update()
    {
        if (GameManagerJ5.gameOver)
        {
            txtGameOver.enabled = true;
            audioS.pitch = 1;
            if (timer2 < 1)
            {
                audioS.PlayOneShot(audioGameOver);
                timer2 = 1;
            }
            Time.timeScale = 0;
            
        }

        if (GameManagerJ5.gameWin)
        {
            txtGameWin.enabled = true;
            audioS.pitch = 10;
            if (timer < 1)
            {
                audioS.PlayOneShot(audioGameOver);
                timer = 1;
            }
            
            Time.timeScale = 0;
        }
    }
}
