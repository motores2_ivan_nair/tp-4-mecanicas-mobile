using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Controller_Player31 : MonoBehaviour
{

    private bool pelotaInicio = false;
    private Rigidbody rb;
    public float velPelotaX = 5f;

    private bool reinicio;
    private void Start()
    {
        Invoke("velInicial", 3);
        reinicio = false;
    }

    private void Update()
    {
        if (pelotaInicio)
        {
            rb = GetComponent<Rigidbody>();

            Vector3 velPelota = rb.velocity;
            velPelota.x = velPelotaX;
            rb.velocity += velPelota*Time.deltaTime;
        }


    }
    private void velInicial()
    {
        GetComponent<Rigidbody>().velocity = new Vector3(10, 0, 0) ;
        pelotaInicio = true;
    }


    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.name== "PisoInvisible"&& !reinicio)
        {
            GameManager31.gameOver = true;
            reinicio = true;
        }

        if (other.gameObject.name == "win")
        {
            GameManager31.gameWin = true;
        }
    }
}
