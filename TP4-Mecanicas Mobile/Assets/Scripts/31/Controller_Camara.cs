using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Controller_Camara : MonoBehaviour
{
    private GameObject player;
    private Vector3 difPos;
    void Start()
    {
        player = GameObject.Find("pelota");

        difPos = transform.position - player.transform.position;
    }

    // Update is called once per frame
    void Update()
    {

        Vector3 nuevaPos=transform.position;
        nuevaPos.x = player.transform.position.x + difPos.x;
        transform.position = nuevaPos;

    }
}
