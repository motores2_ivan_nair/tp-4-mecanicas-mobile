using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class ControllerUI : MonoBehaviour
{
   public  TMPro.TMP_Text txtGameOver;
    public TMPro.TMP_Text txtGameWin;
    void Start()
    {
      txtGameOver.enabled = false;
        txtGameWin.enabled = false;
    }

    // Update is called once per frame
    void Update()
    {
        if (GameManager31.gameOver)
        {
            txtGameOver.enabled = true;
            Time.timeScale = 0;
        }

        if (GameManager31.gameWin)
        {
            txtGameWin.enabled = true;
            Time.timeScale = 0;
        }
    }
}
