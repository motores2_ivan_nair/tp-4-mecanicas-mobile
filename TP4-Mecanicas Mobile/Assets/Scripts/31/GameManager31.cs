using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
public class GameManager31 : MonoBehaviour
{
    public static bool gameOver = false;
    public static bool gameWin = false;
    private void Start()
    {
        gameOver = false;
        gameWin = false;
    }
    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.R))
        {
            Time.timeScale = 1;
            SceneManager.LoadScene("31");
        }
    }
}
