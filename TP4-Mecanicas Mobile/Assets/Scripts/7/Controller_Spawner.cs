using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Controller_Spawner : MonoBehaviour
{
    private float timer = 0;
    public float maxTime = 0;
    public GameObject barras;
    public float altura;

    // Start is called before the first frame update
    void Start()
    {
        //GameObject newBarras = Instantiate(barras);
        //newBarras.transform.position = transform.position + new Vector3(0, Random.Range(-altura, altura), 0);
        //Destroy(newBarras, 12);
    }

    // Update is called once per frame
    void Update()
    {
        if(timer > maxTime)
        {
            GameObject newBarras = Instantiate(barras);
            newBarras.transform.position = transform.position + new Vector3(0, Random.Range(-altura, altura), 0);
            Destroy(newBarras, 12);
            timer = 0;
        }

        timer += Time.deltaTime; 
    }
}
