using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Controller_Player7 : MonoBehaviour
{
    public float jumpForce;
    public TMPro.TMP_Text textoPuntos;
    //public Controller_Hud7 hudController;
    private Rigidbody rb;
    private int puntos;

    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody>();
    }

    // Update is called once per frame
    void Update()
    {
        textoParaPuntos();

        if(Input.GetKeyDown(KeyCode.Space))
        {
            Salto();
        }
    }

    private void textoParaPuntos()
    {
        textoPuntos.text = "Puntaje: " + puntos.ToString();
    }

    private void Salto()
    {
        rb.AddForce(new Vector3(0, jumpForce, 0), ForceMode.Impulse);
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Barras"))
        {
            GameManager7.gameOver = true;
        }

        if (collision.gameObject.CompareTag("Moneda"))
        {
            puntos++;
            textoParaPuntos();
            collision.gameObject.SetActive(false);

            //hudController.actualizarPuntos(puntos);
        }
    }
}
