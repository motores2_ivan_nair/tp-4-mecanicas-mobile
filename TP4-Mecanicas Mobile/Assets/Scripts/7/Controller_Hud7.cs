using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Controller_Hud7 : MonoBehaviour
{
    public TMPro.TMP_Text gameOver;
    public TMPro.TMP_Text restart;
    //public TMPro.TMP_Text textoPuntos;

    // Start is called before the first frame update
    void Start()
    {
        gameOver.enabled = false;
        restart.enabled = false;
    }

    // Update is called once per frame
    void Update()
    {
        if (GameManager7.gameOver)
        {
            gameOver.enabled = true;
            restart.enabled = true;
            Time.timeScale = 0;
        }
    }

    //public void actualizarPuntos(int nuevosPuntos)
    //{
    //    textoPuntos.text = "Puntaje: " + nuevosPuntos.ToString();
    //}
}
