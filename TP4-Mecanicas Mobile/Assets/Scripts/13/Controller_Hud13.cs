using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Controller_Hud13 : MonoBehaviour
{
    public TMPro.TMP_Text txtGameOver;

    public TMPro.TMP_Text txtContadorCajas;

    private GameManager13 gm;
    void Start()
    {
        gm = GameObject.Find("GameManager").GetComponent<GameManager13>();
        txtGameOver.enabled = false;

    }

    // Update is called once per frame
    void Update()
    {
        txtContadorCajas.text = gm.contCajas.ToString();
        if (GameManager13.gameOver)
        {
            txtGameOver.enabled = true;
            gm.GetComponent<AudioSource>().enabled = true;
            Time.timeScale = 0;
        }

       


    }
}
