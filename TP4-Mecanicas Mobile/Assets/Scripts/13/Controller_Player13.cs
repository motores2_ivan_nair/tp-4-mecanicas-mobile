using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Controller_Player13 : MonoBehaviour
{
    public GameObject cajaPrefab;
    private GameManager13 gm;

    private bool poderDisparar=true;
    public int cdDisparo = 1;
    public float velDisparo = 10;


    private AudioSource audioSource;
    void Start()
    {
       gm = GameObject.Find("GameManager").GetComponent<GameManager13>();
        audioSource = GetComponent<AudioSource>();
        audioSource.enabled = false;
    }

    // Update is called once per frame
    void Update()
    {
        if (poderDisparar)
        {
            if (Input.GetKeyDown(KeyCode.Space) || Input.GetKeyDown(KeyCode.Q))
            {
                GameObject cajaInstancia = Instantiate(cajaPrefab, transform.position, Quaternion.identity);
                Rigidbody cajaRb = cajaInstancia.GetComponent<Rigidbody>();

                cajaRb.velocity = new Vector3(velDisparo, 0, 0);
                audioSource.enabled = true;

                gm.contCajas++;
                
                poderDisparar = false;
                Invoke("ResetPoderDisparar",cdDisparo);
            }
        
        }
    }

    private void ResetPoderDisparar()
    {
        poderDisparar = true;
        audioSource.enabled = false;
    }


}
