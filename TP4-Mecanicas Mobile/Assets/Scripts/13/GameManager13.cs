using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
public class GameManager13 : MonoBehaviour
{
    public static bool gameOver=false;

    public int contCajas=0;

    private AudioSource audioSource;

    void Start()
    {
        gameOver = false;
        contCajas = 0;
        audioSource = GetComponent<AudioSource>();
        audioSource.enabled = false;
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.R))
        {
            Time.timeScale = 1;
            SceneManager.LoadScene("13");
        }
    }


}
