using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Controller_Hud34 : MonoBehaviour
{
    public TMPro.TMP_Text gameOver;
    public TMPro.TMP_Text restart;

    // Start is called before the first frame update
    void Start()
    {
        gameOver.enabled = false;
        restart.enabled = false;
    }

    // Update is called once per frame
    void Update()
    {
        if (GameManager34.gameOver)
        {
            gameOver.enabled = true;
            restart.enabled = true;
            Time.timeScale = 0;
        }
    }
}
