using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Controller_Instanciador : MonoBehaviour
{
    public float tiempo;
    public GameObject cuboPJ;

    // Start is called before the first frame update
    void Start()
    {
        InvokeRepeating("instanciarCubos", 0f, tiempo);
    }

    // Update is called once per frame
    void Update()
    {

        if (Input.GetKeyDown(KeyCode.W))
        {
            instanciarCubos();
        }
    }

    private void instanciarCubos()
    {
        Vector3 randomPosition = new Vector3(UnityEngine.Random.Range(-11f, 11f), UnityEngine.Random.Range(3f, 5f), 0);

        Instantiate(cuboPJ, randomPosition, Quaternion.identity);
    }
}
