using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class Controller_CambioColor : MonoBehaviour
{
    [Header("Datos")]
    public Color[] colors;
    public float fuerza;

    [Header("Verificar Bool")]
    public bool azul;
    public bool naranja;
    private Rigidbody rb;

    // Start is called before the first frame update
    void Start()
    {
        Renderer renderer = GetComponent<Renderer>();
        renderer.material.color = colors[1];
        rb = GetComponent<Rigidbody>();
        azul = false; naranja = true;
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void OnMouseDown()
    {
        Renderer renderer = GetComponent<Renderer>();
        Color currentColor = renderer.material.color;

        // Buscar el �ndice del color actual en el array de colores
        int currentIndex = Array.IndexOf(colors, currentColor);

        // Calcular el �ndice del pr�ximo color
        int nextIndex = (currentIndex + 1) % colors.Length;

        // Asignar el pr�ximo color al cubo
        renderer.material.color = colors[nextIndex];

        if (currentColor == colors[0]) //Naranja
        {
            rb.useGravity = true;
            naranja = true;
            azul = false;
        }
        else if (currentColor == colors[1]) //Azul
        {
            rb.useGravity = false;
            rb.AddForce(Vector3.up * fuerza * 2, ForceMode.Force);
            naranja = false;
            azul = true;
        }
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Piso"))
        {
            if (naranja)
            {
                GameManager34.gameOver = true;
            }
        }
    }
}
